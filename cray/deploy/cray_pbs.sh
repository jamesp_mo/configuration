#!/usr/bin/env bash

trap 'echo "Aborted!"; exit 1' ERR
set -e

PROG=$(basename ${0} .sh)

# Check the availability of SCRATCH.
PROBE=$([ -d ${SCRATCH} ] && echo "true" || echo "false")
[ "${PROBE}" == "false" ] && export SCRATCH=${LOCALTEMP}

# Set up LOGFILE for output of this script.
LOGDIR="${SCRATCH}/logs"
mkdir -p ${LOGDIR}

# Get the directory of this script.
SCRIPT_DIR=$(cd "$(dirname ${0})"; pwd)

################################################################################
oops() {
    local EMSG="${1}"

    echo -e "\n${PROG}: ${EMSG}\n"

    exit 1
}

[ ${#} -ne 1 ] && oops "Require CRAY <hostname> command line argument."
TARGET_REMOTE="${1}"

################################################################################
CRAY_SCRIPT="cray_deploy.sh"
MODULE_SCRIPT="create_modulefile.py"

LOGFILE="${LOGDIR}/${PROG}.${TARGET_REMOTE}.$(date +%Y-%m-%d-%H%M%S).log"

# Set up CRAY_LOGFILE for output of the PBS job.
CRAY_WORKDIR=$(ssh -o BatchMode=yes ${TARGET_REMOTE} 'echo ${DATADIR}')/conda_deploy_${TARGET_REMOTE}
CRAY_LOGDIR="${CRAY_WORKDIR}/logs"
CRAY_LOGFILE="${CRAY_LOGDIR}/cray_pbs_deploy.${TARGET_REMOTE}.$(date +%Y-%m-%d-%H%M%S).log"
ssh -o BatchMode=yes ${TARGET_REMOTE} "mkdir -p ${CRAY_LOGDIR}" >${LOGFILE} 2>&1

# Transfer scripts that will be run on the cray.
scp ${SCRIPT_DIR}/${CRAY_SCRIPT} ${SCRIPT_DIR}/${MODULE_SCRIPT} ${TARGET_REMOTE}:${CRAY_WORKDIR} >>${LOGFILE} 2>&1

# Capture standard output + error from this script and cray PBS job.
ssh -o BatchMode=yes ${TARGET_REMOTE} "qsub -j oe -o ${CRAY_LOGFILE} -v TARGET_HOST=${TARGET_REMOTE} ${CRAY_WORKDIR}/${CRAY_SCRIPT}" >>${LOGFILE} 2>&1

echo "Log for the latest deployment can be found on ${TARGET_REMOTE}:${CRAY_LOGFILE}." >>${LOGFILE} 2>&1
