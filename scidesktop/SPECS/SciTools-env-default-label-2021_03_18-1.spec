Name:           SciTools-env-default-label-2021_03_18-1
Version:        3
Release:        0
Summary:        A SciTools environment for default/2021_03_18-1

License:        BSD 3
URL:            http://link/to/gh
BuildRoot:      %{_tmppath}/env-default-label-2021_03_18-1-3

Requires: SciTools-env-default-tag-2021_03_18-1

%description
This is the default/2021_03_18-1 SciTools environment.

%prep
# Clear up any pre-existing build-root.
rm -rf $RPM_BUILD_ROOT

%install
mkdir -p $RPM_BUILD_ROOT/usr/share/Modules/modulefiles/scitools
cat <<'EOF1' > $RPM_BUILD_ROOT/usr/share/Modules/modulefiles/scitools/default-2021_03_18-1
#%Module1.0
#
# scitools - scientific software stack for the "default/2021_03_18-1" environment
#

set ENV_NAME "default-2021_03_18-1"
set LINK_DIR "/opt/scitools/environments/default/2021_03_18-1"
set BASE_DIR "/opt/scitools/environments/default/2021_03_18-1"
set HELP "Scientific software stack for the 'default/2021_03_18-1' environment."

proc ModulesHelp { } {
    global ENV_NAME
    global LINK_DIR
    global BASE_DIR
    global HELP

    puts stderr $HELP
    puts stderr ""
    puts stderr "This environment is available at the base directory $BASE_DIR"
    puts stderr ""
    puts stderr "As new software is deployed to the scientific software stack, this environment"
    puts stderr "WILL NOT update to include the newer versions."
    puts stderr "This environment will remain immutable until removed, however it"
    puts stderr "MAY BE REMOVED WITHOUT WARNING once it has become obsolete."
    puts stderr ""
    puts stderr "For an environment that will always be available, consider using a label
    puts stderr "such as default-current."
    puts stderr ""
    puts stderr "For documentation, see http://www-avd/sci/software_stack/"
}

module-whatis $HELP
conflict scitools um_tools
prepend-path PATH $BASE_DIR/bin
setenv SSS_ENV_DIR $LINK_DIR
setenv SSS_ENV_NAME $ENV_NAME
setenv SSS_TAG_DIR $BASE_DIR
EOF1

# This phase just tidies up after itself.
%clean
rm -rf $RPM_BUILD_ROOT

%files
# All files in this directory are owned by this RPM.
/usr/share/Modules/modulefiles/scitools/default-2021_03_18-1