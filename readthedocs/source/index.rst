.. include:: globals.rst.inc

Scientific Software Stack Documentation
=======================================

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Getting started

   getting_started
   best_practice
   troubleshooting
   cheat_sheet
   roadmap
   feedback


.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Environments
   :glob:

   environments
   environments_available
   environments_compare
   extending_environments
   platforms
   production_use
   release_log


.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Packages

   package_policy
   package_requests
   sss-browser/autogen-sss-packages-used


.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Help & Reference

   usage_logs
   testing
   faq
   eclipse_and_pydev
   glossary
   tools
   contact


.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Related Documentation

   Dask Best Practice <http://www-avd/sci/dask_best_practice>


.. image:: images/sss-logo.png
   :align: right


The Scientific Software Stack is a collection of **Python-based**
software environments that deliver scientific libraries to Met Office Linux
systems.  The aim is to provide sufficiently up to date environments to allow
users (predominantly scientists) to use modern Python tools that are consistent
across all platforms.

A range of libraries in numerous languages are provided in the Scientific
Software Stack, with a primary programming interface in Python. It supports
programming with widely used tools such as `iris`_, `matplotlib`_, `numpy`_
and specialised Met Office tools.

The Scientific Software stack is available on the scientific desktops,
servers, and the Cray XC40 HPC. It contains a number of software
environments, each designed for a specific purpose (e.g. production).
For more information on the platforms see
`Environment Browser <environments_available>`_.


Stack Contents
--------------

The Scientific Software Stack provides libraries which are contained
in a set of environments.  The environments provided by the Stack are
listed below:

* ``default-next``
* ``default-current``
* ``default-previous``
* ``default-20XX_XX_XX-X``
* ``experimental-current``
* ``preproduction-osXX``
* ``production-osXX-X``

For more details of the environments available for each platform
and their contents please see the
`Environments Browser <environments_available>`_.

Each environment has its own particular intended use. The contents of the
environments (libraries and versions of libraries) differ based on their intended
purpose, though they are all similar in content.

The update frequency of an `environment <environments#environment-description>`_
is dependent on its intended purpose.  For each
`platform <platforms>`_ there is a 
`deployment method and window <platforms#deployment-method-window>`_ for
updates.

.. choosing_an_env:

Choosing the Right Environment
==============================

The Scientific Software Stack provides two different types of environments:

1. **Labelled environments.**  Example: ``default``, ``default-next``.  Loading one of these
   environments, *you are guaranteed to sucessfully load a working environment, 
   but it will change over time*.
   As new packages or package versions become available, these labelled environments 
   will be updated to include newer versions.  See :ref:`environment-labels`.
2. **Static environments.** Example: ``default-2021_03_18-1``, ``production-os44-1``.  
   Loading one of these environments, *you are guaranteed the same exact set of packages
   every time, but the environment will disappear when obsolete*.

There are trade-offs to using one or the other of these types of environments depending
on the use case.  Static environments can be useful for running non-interactive 
batch jobs over long periods of time over which you want to ensure a consistent baseline. 
However, only running with a static environment may lead to a large accumulation of 
technical debt when the environment is obsolete and removed.

Labelled environments are recommended for development and general purpose data analysis.
Using a labelled environment can ensure that your software is incrementally
updated to match the latest, most stable and secure dependencies, spreading the maintenance
load over time. 

For advice on which environment to use, feel free to contact AVD.

Production Environments
=======================

All Production Environments are *static environments*, you can expect the  
available packages and versions to remain exactly constant for the entire lifetime 
of the environment.

The name of the production environment is comprised of the name of the Operational
Suite that it is associated with, followed by the build number.
For example, the production environment for os40 will be called ``production-os40-1``.

More detail on Stack environments is available on the
`environments <environments>`_ page.



.. _iris: https://github.com/SciTools/iris
.. _matplotlib: https://github.com/matplotlib/matplotlib
.. _numpy: https://github.com/numpy/numpy
