.. include:: globals.rst.inc

Environments Overview
=====================


The Scientific Software Stack provides a number of software environments. Each
environment is a complete, self-contained deployment. It centres
around a Python executable, which can import and use all the Python
packages installed within the environment. The environment also contains all
the associated libraries and binaries required by the bundled Python packages.

The Scientific Software Stack provides a series of environments to meet various
user needs simultaneously.

The update frequency will vary across environments, and the process for
releasing
environments will vary across Met Office Linux systems.
New environment releases will be announced on the
`AVD Yammer Group <https://www.yammer.com/metoffice.gov.uk/topics/28360539#/Threads/AboutTopic?type=about_topic&feedId=28360539>`_.


Environment Variables
---------------------

Each environment has several environment variables associated with
it, which can be used as described in the table below:

:SSS_ENV_DIR:

* Provides a **symbolic link** to the loaded environment
* The symbolic link provides a **constant reference** to the chosen
  environment, which will change underneath the symbolic link as new
  environments are deployed
* Usage example: ``${SSS_ENV_DIR}/lib`` or ``${SSS_ENV_DIR}/include``


:SSS_TAG_DIR:

* Provides the absolute directory of the loaded environment
* This provides an absolute reference to the chosen environment for
  reproducibility (references immutable environment path)
* Usage example:  ``${SSS_TAG_DIR}/lib`` or ``${SSS_TAG_DIR}/include``



Environment Names and Roles
---------------------------

The name of an environment is made up of two parts; the **description** (such
as ``default``), which tells us what the environment should be used for, and
the **label** (such as ``next``), which tells us how recently the environment
was released.

Providing environments that are named with this convention gives us two degrees
of variation between environment content:

* Environments with a **different description** are likely to have larger
  differences between the libraries available within them.
* Environments with the **same description** (for example, all ``default``
  environments) will have largely the same libraries available within them.
* Environments with the **same description and different label** are likely to
  only have differences in library versions.    For example, ``default-next`` is
  likely to have more up-to-date library versions than ``default-current``.

This naming convention should enable a user to decide which environment is
appropriate for their uses.  For example if a user is writing a script which
is intended for production use, then they should be using an environment which
has the description ``production`` and a label matching the intended production
suite.  On the other hand, if a user is writing a Python script for research
and would like to use the most cutting-edge software available (regardless of
whether it is supported or not), then the ``experimental`` environment may be
appropriate.  These environments will continue to be updated to newer packages
and latest versions as they become available.

**Static labels** are available for the following environments:

* All ``production`` environments.  By design, all production environments are 
  static and immutable once released.  If a production environment needs to
  be updated, a new build number will be produced, and all build numbers will
  continue to be supported.
* All ``default`` environments.  When a new environment is released as ``default-next``,
  it can also be loaded from a dated static environment label.  For example, 
  on 18 March 2021 a new ``default`` environment was released.  This can be loaded with
  either of the following commands:

  .. code-block:: bash

       $ module load scitools/default-next

       $ module load scitools/default-2021_03_18-1

  The ``default-2021_03_18-1`` label will continue to point to the *exact same environment*
  for its lifetime, as it becomes ``default-current`` and then ``default-previous`` 
  (see :ref:`label-transitions`).  Once
  an environment loses the ``default-previous`` label, it is considered obsolete
  and will no longer be available at the ``default-2021_03_18-1`` label either.

  

Environment Descriptions
------------------------

Every Stack environment available on all Met Office Linux systems will
follow one of these environment descriptions:


``default``
^^^^^^^^^^^

.. list-table::
  :widths: 3 10

  * - **Description**
    - A general purpose Python3 environment with many useful packages
  * - **Intended Purpose**
    - Best suited for most user Python3 needs
  * - **Production Ready**
    - No
  * - **Update Cycle**
    - Approximately monthly
  * - **Support**
    - **Development**; **stable** and mutable, aim to fix within 1 working week


``default_legacy``
^^^^^^^^^^^^^^^^^^

.. list-table::
   :widths: 3 10

   * - **Active**
     - **No**.  ``default_legacy`` environments will be removed |br|
       from the Met Office computing estate at the end of May 2021.
   * - **Description**
     - A general purpose Python2 environment with many useful packages
   * - **Intended Purpose**
     - Best suited for most user Python2 needs
   * - **Production Ready**
     - No
   * - **Update Cycle**
     - Approximately monthly
   * - **Support**
     - **Development**; **stable** and mutable, aim to fix within 1 working week



``experimental``
^^^^^^^^^^^^^^^^

.. list-table::
   :widths: 3 10

   * - **Description**
     - Contains a wide range of cutting-edge **Python3** packages
   * - **Intended Purpose**
     - Using the latest and greatest with Python3
   * - **Production Ready**
     - No
   * - **Update Cycle**
     - Frequent
   * - **Support**
     - **Development**; **unstable** and mutable, aim to fix only when |br|
       possible and at low priority


``experimental_legacy``
^^^^^^^^^^^^^^^^^^^^^^^

.. list-table::
  :widths: 3 10

  * - **Active**
    - **No**.  ``experimental_legacy`` environments will be removed |br|
      from the Met Office computing estate at the end of May 2021.
  * - **Description**
    - Contains a wide range of cutting-edge **Python2** packages
  * - **Intended Purpose**
    - Using the latest and greatest with Python2
  * - **Production Ready**
    - No
  * - **Update Cycle**
    - Frequent
  * - **Support**
    - **Development**; **unstable** and mutable, aim to fix only when  |br|
      possible and at low priority


``preproduction``
^^^^^^^^^^^^^^^^^

.. list-table::
  :widths: 3 10

  * - **Description**
    - Contains stable **Python3** packages that are primed for production
  * - **Intended Purpose**
    - For Python3 developers targeting a forthcoming operational  |br|
      environment. It will be removed when the associated ``production``  |br|
      environment is made available
  * - **Production Ready**
    - No
  * - **Update Cycle**
    - In line with Operational Suite change management
  * - **Support**
    - **Development**; **stable** and mutable, will fix as soon as  |br|
      possible at high priority


``preproduction_legacy``
^^^^^^^^^^^^^^^^^^^^^^^^

.. list-table::
  :widths: 3 10

  * - **Description**
    - Contains limited stable **Python2** packages that are primed for |br|
      production
  * - **Intended Purpose**
    - For Python2 developers targeting a forthcoming operational  |br|
      environment.  It will be removed when the associated ``production``  |br|
      environment is made available
  * - **Production Ready**
    - No
  * - **Update Cycle**
    - In line with Operational Suite change management
  * - **Support**
    - **Development**; **stable** and mutable, will fix as soon as possible  |br|
      at high priority


``production``
^^^^^^^^^^^^^^

.. list-table::
  :widths: 3 10

  * - **Description**
    - Contains limited stable **Python3** that have been ratified for  |br|
      production use
  * - **Intended Purpose**
    - Python3 packages provisioned for the Operational Suites and Linux  |br|
      servers
  * - **Production Ready**
    - Yes
  * - **Update Cycle**
    - In line with Operational Suite change management
  * - **Support**
    - **Operational**; **stable** and immutable, will fix as soon as |br|
      possible at high priority by deploying a new production environment


``production_legacy``
^^^^^^^^^^^^^^^^^^^^^

.. list-table::
  :widths: 3 10

  * - **Description**
    - Contains limited stable **Python2** that have been ratified for |br|
      production use
  * - **Intended Purpose**
    - Python2 packages provisioned for the Operational Suites and Linux  |br|
      servers
  * - **Production Ready**
    - Yes
  * - **Update Cycle**
    - In line with Operational Suite change management
  * - **Support**
    - **Operational**; **stable** and immutable, will fix as soon as |br|
      possible at high priority by deploying a new production environment

.. _environment-labels:

Environment Labels
------------------

In addition to the above descriptions, environments can be further defined by
the following labels:

* ``current``: the currently deployed and maintained version of the environment
* ``next``: the proposal for the next maintained version of the environment
* ``previous``: the previous maintained version of the environment
* ``osXX-X``: the current production environment label, for example
  ``os40``.  Any essential changes to this environment will lead to a new build
  of the environment being released under the label i.e. ``os40-1`` then
  ``os40-2`` etc

Production environment labels are based on the Operational Suite release
version.

The various labels provided for each description allow us to do the following:

* Prime the next environment that will become the ``current``
  environment in the future. Hence:

    * More up-to-date versions of libraries can be released on a more rapid
      timescale than may be appropriate for the ``current`` label.
    * Users can preview how their code works with the upcoming current version.
      This means any updates that are needed in user code can be made before
      the new label becomes current.
* Maintain an old ``previous`` version of the environment. This
  maintains the previous current label until the next update of the environment.

.. _label-transitions:

Environment Label Transitions
------------------------------

Label transitions happen as follows. In this example we follow the ``default``
environment as two updates are made to the versions available in the
environment.


.. figure:: images/env-label-transition.png
   :align: center

   **Label Transitions**

.. note:: Note that after each ``default`` update the  ``previous`` version is
         retired and no longer accessible via a label.  The environment is considered
         retired. 
         **At this point, the static labelled environment is also removed and no longer
         acessible.**


Environment Availability
------------------------

Stack availability will change frequently as new environments are released
and old ones are simultaneously retired.  Where possible, environment updates
will occur in parity between platforms.  Users will be alerted of events and
any potential issues in the continuous progression of the Stack with
`announcements on Yammer <https://www.yammer.com/metoffice.gov.uk/topics/28360539#/Threads/AboutTopic?type=about_topic&feedId=28360539>`_.

Please note that you will not be able to access every environment that has been
released at all times, as only a small number of environments will be
accessible via ``module load`` at any given time.  If you do need to use an
environment which has been retired, please `Contact AVD <contact>`_ with
your request.

For information on which environments are available on which Met Office Linux
platforms, and for information on the contents of different environments, see
the `environments browser <environments_available>`_ page.


Environment Package Parity
--------------------------

In order to promote a migration from **Python2** to **Python3** that is as
smooth as possible for users, AVD will endeavour to ensure that the same
packages at the same versions are available in similar Python2 and Python3
environments. For example, the same packages and versions in the
``experimental_legacy-current`` Python2 environment will be available in the
``experimental-current`` Python3 environment.

Unfortunately, there will be package exceptions between similar Python2 and
Python3 environments, as many developers have ceased development and support
of their package for Python2. Also, there are now many new packages available
in Python3 that are not available in Python2.
