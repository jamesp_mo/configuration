.. include:: globals.rst.inc


.. _environment_browser:

Environment Browser
===================

The table below of Scientific Software Stack :term:`environments <environment>`
are deployed to the `platforms <platforms>`_ as described
`here <platforms>`_.  This table includes a short list of core packages
for quick reference.  Note the **Description-Tag** and the **Core Packages**
are links to show more detail.

Each environment has a corresponding :term:`manifest` that is stored in
Bitbucket.  To view this explore the environment by selecting the
**Description-Tag**.


.. include:: sss-browser/autogen-sss-env-list.rst.inc


If you want to browse the packages available you can
do this via the `package search <sss-browser/autogen-sss-packages-used.html>`_.


.. note:: The above table was **auto-generated** and only shows the latest
          state of the the environment definition.  The environment available
          on a specific machine may differ slightly as the release may not
          have happened yet.  We aim to ensure all environments are updated
          at the same time, typically within a 24h period.


For the Operational Suite ``production`` and ``production_legacy`` environments,
only the last **three** environments will be available.  For example, if ``os40-1``,
``os41-1`` and ``os42-1`` are available, and ``os43-1`` is then deployed, the
``os40-1`` will be decommissioned.

