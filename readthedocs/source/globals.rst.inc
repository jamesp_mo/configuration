.. _Bitbucket: https://bitbucket.org/metoffice/configuration/src/master/readthedocs
.. |bb_url_replace| replace:: https://bitbucket.org/metoffice/configuration/src/master/readthedocs
.. |br| raw:: html

  <br/>

:bitbucket_url: |bb_url_replace|