#!/usr/bin/bash

# This script can be called from conda-execute:
#
# $ ~avd/live/conda-execute/bin/conda-execute -vf build.sh

# conda execute
# env:
#  - jinja2
#  - requests
#  - pip

pip install sphinx
pip install sphinx_rtd_theme
pip install sphinx-copybutton
pip install asciinema==2.0.2

# capture command line options so we can pass it thru
CLA_OPTIONS=$*

# first time use
mkdir source/sss-browser 2>/dev/null

echo "==> Clean the jinja templates output.."

if [ -d "source/sss-browser/" ]
then
    rm -f source/sss-browser/autogen-*
else
    echo "Error, source/sss-browser directory missing"
    exit 1
fi

# Clear the build output from the readthedocs make
make clean

# parse the jinja templates
echo "==> Creating rst from templates..."

# Can use a cache via "--USE_CACHE"
python sss-browser.py ${CLA_OPTIONS}

RETURN_CODE=${?}

if [ ${RETURN_CODE} -ne 0 ]
then
    echo "sss-browser.py failed.  Build Aborted!"
    exit ${RETURN_CODE}
fi

# create the docs
make html
